/*
 * =====================================================================================
 *
 *       Filename:  QDateDisplay.cpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 10:57:01
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QCalendarWidget>
#include <QHBoxLayout>
#include <QLabel>

#include "QAnalogClock.hpp"
#include "QDigitalClock.hpp"
#include "QDateDisplay.hpp"

QDateDisplay::QDateDisplay(QWidget *parent) : QDockWidget("Date", parent) {
	setWidget(new QCalendarWidget());
}

void QDateDisplay::update() {
}

