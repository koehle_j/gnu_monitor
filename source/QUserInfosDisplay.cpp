/*
 * =====================================================================================
 *
 *       Filename:  QUserInfosDisplay.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 23:43:42
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QFormLayout>

#include "QUserInfosDisplay.hpp"

QUserInfosDisplay::QUserInfosDisplay(QWidget *parent) : QDockWidget("User infos", parent) {
	auto *layoutWidget = new QWidget;
	setWidget(layoutWidget);

	auto *layout = new QFormLayout(layoutWidget);
	layout->addRow("Machine name:", &m_hostnameLabel);
	layout->addRow("User name:", &m_usernameLabel);
}

void QUserInfosDisplay::update() {
	m_info->FillData();

	m_hostnameLabel.setText(m_info->getData().at(0).at("hostname").c_str());
	m_usernameLabel.setText(m_info->getData().at(0).at("name").c_str());
}

