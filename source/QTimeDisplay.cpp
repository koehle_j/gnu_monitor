/*
 * =====================================================================================
 *
 *       Filename:  QTimeDisplay.cpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 10:58:16
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QCalendarWidget>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QLabel>

#include "QTimeDisplay.hpp"

QTimeDisplay::QTimeDisplay(QWidget *parent) : QDockWidget("Time", parent) {
	auto *layoutWidget = new QWidget;
	setWidget(layoutWidget);

	auto *layout = new QVBoxLayout(layoutWidget);
	layout->addWidget(&m_analogClock);
	layout->addWidget(&m_digitalClock);

	auto *checkBox = new QCheckBox("Analogic");
	layout->addWidget(checkBox);

	m_analogClock.hide();

	connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(changeClockDisplay(int)));
}

void QTimeDisplay::update() {
}

void QTimeDisplay::changeClockDisplay(int state) {
	if (state == Qt::Checked) {
		m_analogClock.setVisible(true);
		m_digitalClock.setVisible(false);
	}
	else {
		m_analogClock.setVisible(false);
		m_digitalClock.setVisible(true);
	}
}

