
#include "Network.hpp"

Network::Network()
  : AMonitorModule("Network", "/proc/net/if_inet6", 1)
{
}

Network::Network(AMonitorModule const &t)
  : AMonitorModule("Network", "/proc/net/if_inet6", 1)
{
  if (this != &t)
    m_data = t.getData();
}

Network		&Network::operator=(AMonitorModule const &t)
{
  if (this != &t)
    m_data = t.getData();
  return (*this);
}

Network::~Network()
{
}

std::string		Network::getAddress(std::string ip)
{
  int			pos = 4;

  while (pos <= 35)
    {
      ip.insert(pos, ":");
      pos += 5;
    }
    return (ip);
}

void			Network::FillData()
{
  std::map<std::string, std::string> map;
  std::string		line;
  std::string		ip;
  std::string		interface;
  std::ifstream		fs(m_filename, std::istream::in);

  if (!fs.is_open())
    {
      std::cerr << m_name << ": Error xhile opening `" << m_filename << "`" << std::endl;
      return ;
    }
  while (std::getline(fs, line))
    {
      ip = getAddress(line.substr(0, line.find(' ')));
      interface = line.substr(line.rfind(' ') + 1);
        map.insert(std::pair<std::string, std::string>(interface, ip));
    }
  m_data.push_back(map);
}
