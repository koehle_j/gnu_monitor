/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 21:32:29
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include "Application.hpp"

int main(int argc, char **argv) {
	Application app(argc, argv);

	return app.exec();
}

