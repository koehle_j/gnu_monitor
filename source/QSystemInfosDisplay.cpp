/*
 * =====================================================================================
 *
 *       Filename:  QSystemInfosDisplay.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 23:43:42
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QFormLayout>

#include "QSystemInfosDisplay.hpp"

QSystemInfosDisplay::QSystemInfosDisplay(QWidget *parent) : QDockWidget("System infos", parent) {
	auto *layoutWidget = new QWidget;
	setWidget(layoutWidget);

	auto *layout = new QFormLayout(layoutWidget);
	layout->addRow("Kernel version:", &m_kernelVersionLabel);
	layout->addRow("GCC version used:", &m_gccVersionLabel);
	layout->addRow("Operating system:", &m_operatingSystemLabel);
}

void QSystemInfosDisplay::update() {
	m_info->FillData();

	m_kernelVersionLabel.setText(m_info->getData().at(0).at("version").c_str());
	m_gccVersionLabel.setText(m_info->getData().at(0).at("gcc").c_str());
	m_operatingSystemLabel.setText(m_info->getData().at(0).at("system").c_str());
}

