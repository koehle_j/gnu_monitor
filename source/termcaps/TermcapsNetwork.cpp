//
// TermcapsNetwork.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <algorithm>
#include "TermcapsNetwork.hpp"

Termcaps::Network::Network(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(5, 50, y, x);
}

Termcaps::Network::~Network() {
  delwin(m_win);
}

void Termcaps::Network::update() {
  m_info->FillData();
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 0, 0, "NETWORK:");
  wattroff(m_win, A_BOLD);
  wattron(m_win, COLOR_PAIR(5));
  displayInfo(m_info->getData());
  wattron(m_win, COLOR_PAIR(1));
  wrefresh(m_win);
}

void Termcaps::Network::setModuleInfo(IMonitorModule *info) {
  m_info.reset(info);
}

void Termcaps::Network::displayInfo(std::vector<std::map<std::string, std::string>> const &data) const {
  int line = 1;

  for (auto &it : data.at(0)) {
    mvwprintw(m_win, line, 0, "%s: ", it.first.c_str());
    wattron(m_win, A_BOLD);
    mvwprintw(m_win, line, it.first.length() + 2, "%s", it.second.c_str());
    wattroff(m_win, A_BOLD);
    ++line;
  }
}
