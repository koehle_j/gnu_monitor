//
// TermcapsDate.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <algorithm>
#include <QTime>
#include <QDate>
#include "TermcapsDate.hpp"

Termcaps::Date::Date(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(2, 32, y, x);
}

Termcaps::Date::~Date() {
  delwin(m_win);
}

void Termcaps::Date::update() {
  QTime time = QTime::currentTime();
  std::string time_text = time.toString("hh:mm:ss").toStdString();
  QDate date = QDate::currentDate();
  std::string date_text = date.toString("dd/MM/yyyy").toStdString();

  if ((time.second() % 2) == 0) {
    time_text[2] = ' ';
    time_text[5] = ' ';
  }
  wattron(m_win, COLOR_PAIR(5));
  mvwprintw(m_win, 0, 0, "Date:");
  mvwprintw(m_win, 1, 0, "Time:");
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 0, 6, date_text.c_str());
  mvwprintw(m_win, 1, 6, time_text.c_str());
  wattroff(m_win, A_BOLD);
  wattron(m_win, COLOR_PAIR(1));
  wrefresh(m_win);
}

void Termcaps::Date::setModuleInfo(IMonitorModule *info) {
  m_info.reset(info);
}
