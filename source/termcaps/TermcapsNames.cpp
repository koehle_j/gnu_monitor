//
// TermcapsNames.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <algorithm>
#include "TermcapsNames.hpp"

Termcaps::Names::Names(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(2, 32, y, x);
}

Termcaps::Names::~Names() {
  delwin(m_win);
}

void Termcaps::Names::update() {
  m_info->FillData();
  wattron(m_win, COLOR_PAIR(5));
  mvwprintw(m_win, 0, 0, "Machine name:");
  mvwprintw(m_win, 1, 0, "User name:");
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 0, 14, "%s", m_info->getData().at(0).at("hostname").c_str());
  mvwprintw(m_win, 1, 14, "%s", m_info->getData().at(0).at("name").c_str());
  wattroff(m_win, A_BOLD);
  wattron(m_win, COLOR_PAIR(1));
  wrefresh(m_win);
}

