//
// GlobalInfo.cpp for global info in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sun Jan 22 08:19:56 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include "GlobalInfo.hpp"

GlobalInfo::GlobalInfo() : AMonitorModule("GLobalInfo", "/proc/version", 1)
{
}

GlobalInfo::~GlobalInfo()
{
}

GlobalInfo &GlobalInfo::operator=(AMonitorModule const &other)
{
  m_data = other.getData();
  return (*this);
}

static std::string getWord(std::string line, int i)
{
  int	count = 0;
  for (unsigned int j = 0; j < line.length(); j++)
    {
      if (line[j] == ' ')
	count++;
      if (i == 0)
	return (line.substr(j, line.find(" ", j + 1) - j));
      if (count == i)
	return (line.substr(j + 1, line.find(" ", j + 1) - j - 1));
    }
  return (line);
}

void GlobalInfo::FillData()
{
  std::ifstream fd(m_filename, std::fstream::in);
  std::ifstream fd2("/etc/os-release", std::fstream::in);
  std::map<std::string, std::string> map;
  std::string line;
  std::string line2;

  if (getSizeHistory() >= m_max)
    m_data.erase(m_data.begin());
  if (!fd.is_open())
    {
      std::cout << m_name << ": Error while opening `" << m_filename << "`" << std::endl;
      return ;
    }
  getline(fd, line);
  getline(fd2, line2);
  int	pos = line2.substr(6).find("\"");
  map["system"] = line2.substr(6, pos);
  // map["system"][line2.length() - 1] = 0;
  map["version"] = getWord(line, 2);
  map["gcc"] = getWord(line, 6) + " " + getWord(line, 7);
  // map["system"] = getWord(line, 0);
  m_data.push_back(map);
  fd.close();
  fd2.close();
}
