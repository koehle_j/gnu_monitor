/*
 * =====================================================================================
 *
 *       Filename:  QtMonitorRenderer.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 20:54:32
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QApplication>
#include <QCloseEvent>
#include <QDesktopWidget>
#include <QDockWidget>
#include <QKeyEvent>
#include <QStyle>

#include "QtMonitorRenderer.hpp"

QtMonitorRenderer::QtMonitorRenderer() : QMainWindow(nullptr, Qt::Dialog) {
	setWindowTitle("gkrellm");
	setFocusPolicy(Qt::ClickFocus);
	resize(1600, 900);

	setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));
}

void QtMonitorRenderer::update() {
	for (auto &it : m_modules) {
		it.second->update();
	}
}

void QtMonitorRenderer::setupModule(size_t hashCode, Qt::DockWidgetArea area, Qt::Orientation orientation) {
	addDockWidget(area, dynamic_cast<QDockWidget *>(m_modules.at(hashCode)), orientation);
}

void QtMonitorRenderer::keyPressEvent(QKeyEvent *event) {
	if (event->key() == Qt::Key_Escape) {
		close();
	}
}

