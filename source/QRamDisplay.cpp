/*
 * =====================================================================================
 *
 *       Filename:  QRamDisplay.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:29:19
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QChart>
#include <QChartView>

#include "QRamDisplay.hpp"

QRamDisplay::QRamDisplay(QWidget *parent) : QDockWidget("RAM", parent) {
	m_series = new QtCharts::QLineSeries();
	// m_series->append(0, 6);
	// m_series->append(2, 4);
	// m_series->append(3, 8);
	// m_series->append(7, 4);
	// m_series->append(10, 5);
	// *m_series << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);

	QtCharts::QChart *chart = new QtCharts::QChart();
	chart->legend()->hide();
	chart->addSeries(m_series);
	chart->createDefaultAxes();
	chart->axisX()->setLabelsVisible(false);
	chart->axisX()->setRange(0, 29);
	chart->axisY()->setRange(0, 100);
	chart->setBackgroundVisible(false);

	QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	setWidget(chartView);
}

void QRamDisplay::update() {
	m_info->FillData();

	m_series->clear();
	for (size_t i = 0 ; i < 30 && i < m_info->getData().size() ; ++i) {
		int current = std::stoi(m_info->getData().at(i).at("current"));
		int max = std::stoi(m_info->getData().at(i).at("max"));

		m_series->append(i, (float)current / max * 100);
	}
}

