/*
 * =====================================================================================
 *
 *       Filename:  QCpuDisplay.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:29:19
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QChart>
#include <QChartView>

#include "QCpuDisplay.hpp"

QCpuDisplay::QCpuDisplay(QWidget *parent) : QDockWidget("CPU", parent) {
	m_series = new QtCharts::QLineSeries();

	QtCharts::QChart *chart = new QtCharts::QChart();
	chart->legend()->hide();
	chart->addSeries(m_series);
	chart->createDefaultAxes();
	chart->axisX()->setLabelsVisible(false);
	chart->axisX()->setRange(0, 29);
	chart->axisY()->setRange(0, 100);
	chart->setBackgroundVisible(false);
	// chart->setTitle("Simple line chart example");

	QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	setWidget(chartView);
}

void QCpuDisplay::update() {
	m_info->FillData();

	m_series->clear();
	for (size_t i = 0 ; i < 30 && i < m_info->getData().size() ; ++i) {
		double usage = std::stod(m_info->getData().at(i).at("usage"));
		m_series->append(i, usage);
	}
}

