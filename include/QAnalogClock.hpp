/*
 * =====================================================================================
 *
 *       Filename:  QAnalogClock.hpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 00:58:58
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QANALOGCLOCK_HPP_
#define QANALOGCLOCK_HPP_

#include <QWidget>

class QAnalogClock : public QWidget {
	Q_OBJECT

	public:
		QAnalogClock(QWidget *parent = nullptr);

	protected:
		void paintEvent(QPaintEvent *event) override;
};

#endif // QANALOGCLOCK_HPP_
