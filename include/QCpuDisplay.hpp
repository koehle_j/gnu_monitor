/*
 * =====================================================================================
 *
 *       Filename:  QCpuDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:28:01
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QCPUDISPLAY_HPP_
#define QCPUDISPLAY_HPP_

#include <QDockWidget>
#include <QLineSeries>

#include "AMonitorDisplay.hpp"

class QCpuDisplay : public AMonitorDisplay, public QDockWidget {
	public:
		QCpuDisplay(QWidget *parent = nullptr);

		void update() override;

	private:
		QtCharts::QLineSeries *m_series;
};

#endif // QCPUDISPLAY_HPP_
