/*
 * =====================================================================================
 *
 *       Filename:  QUserInfosDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 23:42:46
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QUSERINFOSDISPLAY_HPP_
#define QUSERINFOSDISPLAY_HPP_

#include <QDockWidget>
#include <QLabel>

#include "AMonitorDisplay.hpp"

class QUserInfosDisplay : public QDockWidget, public AMonitorDisplay {
	public:
		QUserInfosDisplay(QWidget *parent = nullptr);

		void update();

	private:
		QLabel m_hostnameLabel;
		QLabel m_usernameLabel;
};

#endif // QUSERINFOSDISPLAY_HPP_
