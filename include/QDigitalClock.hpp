/*
 * =====================================================================================
 *
 *       Filename:  QDigitalClock.hpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 01:17:00
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QDIGITALCLOCK_HPP_
#define QDIGITALCLOCK_HPP_

#include <QLCDNumber>

class QDigitalClock : public QLCDNumber {
	Q_OBJECT

	public:
		QDigitalClock(QWidget *parent = 0);

	private slots:
		void showTime();
};

#endif // QDIGITALCLOCK_HPP_
