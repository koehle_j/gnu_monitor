//
// TermcapsRAM.hpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:44:49 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#ifndef TERMCAPSRAM_H_
# define TERMCAPSRAM_H_

#include <ncurses.h>
#include <string>
#include <vector>
#include <map>
#include "AMonitorDisplay.hpp"

namespace Termcaps {
  class RAM : public AMonitorDisplay {
  public:
    RAM(int x, int y);
    virtual ~RAM();

    virtual void update() override;

    virtual void setModuleInfo(IMonitorModule *info) override;

    void displayGraphic(std::vector<std::map<std::string, std::string>> const &data) const;

  private:
    int m_x;
    int m_y;
    WINDOW *m_win;
  };
}

#endif // TERMCAPSRAM_H_
