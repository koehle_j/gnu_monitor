/*
 * =====================================================================================
 *
 *       Filename:  QRamDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:28:01
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QRAMDISPLAY_HPP_
#define QRAMDISPLAY_HPP_

#include <QDockWidget>
#include <QLineSeries>

#include "AMonitorDisplay.hpp"

class QRamDisplay : public AMonitorDisplay, public QDockWidget {
	public:
		QRamDisplay(QWidget *parent = nullptr);

		void update() override;

	private:
		QtCharts::QLineSeries *m_series;
};

#endif // QRAMDISPLAY_HPP_
