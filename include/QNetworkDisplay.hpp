/*
 * =====================================================================================
 *
 *       Filename:  QNetworkDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:28:01
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QNETWORKDISPLAY_HPP_
#define QNETWORKDISPLAY_HPP_

#include <QDockWidget>

#include "AMonitorDisplay.hpp"

class QNetworkDisplay : public AMonitorDisplay, public QDockWidget {
	public:
		QNetworkDisplay(QWidget *parent = nullptr);

		void update() override;
};

#endif // QNETWORKDISPLAY_HPP_
