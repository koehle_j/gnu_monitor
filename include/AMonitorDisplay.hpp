/*
 * =====================================================================================
 *
 *       Filename:  AMonitorDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 23:15:03
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef AMONITORDISPLAY_HPP_
#define AMONITORDISPLAY_HPP_

#include <memory>

#include "IMonitorDisplay.hpp"

class AMonitorDisplay : public IMonitorDisplay {
	public:
		virtual ~AMonitorDisplay() = default;

		void setModuleInfo(IMonitorModule *info) override { m_info.reset(info); }

	protected:
		std::unique_ptr<IMonitorModule> m_info = nullptr;
};

#endif // AMONITORDISPLAY_HPP_
